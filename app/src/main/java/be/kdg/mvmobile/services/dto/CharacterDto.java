package be.kdg.mvmobile.services.dto;

public class CharacterDto {
    private int id;
    private String characterName;
    private String imgPath;
    private int extraGoldColor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public int getExtraGoldColor() {
        return extraGoldColor;
    }

    public void setExtraGoldColor(int extraGoldColor) {
        this.extraGoldColor = extraGoldColor;
    }
}
