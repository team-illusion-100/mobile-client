package be.kdg.mvmobile.services;

import java.util.List;

import be.kdg.mvmobile.services.dto.FriendDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface FriendService {
    @GET("/getFriends")
    Call<List<FriendDto>> getFriends(@Header("Authorization") String authToken);
}
