package be.kdg.mvmobile.services.dto;

public class DeviceDto {
    private String username;
    private String deviceId;

    public DeviceDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
