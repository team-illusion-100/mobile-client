package be.kdg.mvmobile.services;

import android.annotation.SuppressLint;
import android.util.Log;

import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class WebsocketService {
    private static final String TAG = "WebsocketService";

    private StompClient stompClient;
    private final MySharedPreferences mySharedPreferences;
    private int connectedGameId;

    public WebsocketService(MySharedPreferences mySharedPreferences) {
        this.mySharedPreferences = mySharedPreferences;
    }

    @SuppressLint("CheckResult")
    public void initialiseWebSocketConnection() {
        stompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "http://10.0.2.2:5000/machiavelli/websocket?access_token=" + mySharedPreferences.readTokenSP().getAccess_token());
        stompClient.lifecycle().subscribe(lifecycleEvent -> {
            switch (lifecycleEvent.getType()) {

                case OPENED:
                    Log.d(TAG, "WS OPENED");
                    break;

                case ERROR:
                    Log.d(TAG, "ERROR " + lifecycleEvent.getException());
                    break;

                case CLOSED:
                    Log.d(TAG, "WS CLOSED");
                    break;
            }
        });
        stompClient.connect();
    }

    public StompClient getStompClient() {
        return stompClient;
    }
}
