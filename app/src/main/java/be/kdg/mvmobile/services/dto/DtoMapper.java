package be.kdg.mvmobile.services.dto;

import android.util.Log;
import android.widget.Toast;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import be.kdg.mvmobile.R;
import be.kdg.mvmobile.model.users.Friend;
import be.kdg.mvmobile.model.users.Statistics;
import be.kdg.mvmobile.model.users.User;

public class DtoMapper {
    private static final String TAG = "RegisterActivity";

    public DtoMapper() {
    }

    public UserDto userToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setBirthday(user.getDateOfBirth().toString());
        userDto.setAvatar(user.getAvatar());

        return userDto;
    }

    public User userFromDto(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());

        // Try to parse the incoming birthday to dateOfBirth
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'HH:mm", Locale.FRANCE);

        try {
            user.setDateOfBirth(format.parse(userDto.getBirthday()));
        } catch (ParseException e) {
            Log.e(TAG, "userFromDto: ", e);
            // Initialize with a default date
            user.setDateOfBirth(new Date(1));
        }
        user.setAvatar(userDto.getAvatar());

        return user;
    }

    public FriendDto friendToDto(Friend friend) {
        FriendDto result = new FriendDto();
        result.setId(friend.getFriendId());
        result.setEmail(friend.getEmail());
        result.setUserName(friend.getUsername());
        return result;
    }

    public Friend friendFromDto(FriendDto friendDto) {
        Friend friend = new Friend();
        friend.setFriendId(friendDto.getId());
        friend.setEmail(friendDto.getEmail());
        friend.setUsername(friendDto.getUserName());

        return friend;
    }

    public StatisticsDto statisticsToDto(Statistics statistics) {
        StatisticsDto statisticsDto = new StatisticsDto();
        statisticsDto.setNumberOfGames(statistics.getNumberOfGames());
        statisticsDto.setNumberOfWins(statistics.getNumberOfWins());
        statisticsDto.setTopScore(statistics.getTopScore());
        statisticsDto.setAverageScore(statistics.getAverageScore());

        return statisticsDto;
    }

    public Statistics statisticsFromDto(StatisticsDto statisticsDto) {
        Statistics statistics = new Statistics();
        statistics.setNumberOfGames(statisticsDto.getNumberOfGames());
        statistics.setNumberOfWins(statisticsDto.getNumberOfWins());
        statistics.setTopScore(statisticsDto.getTopScore());
        statistics.setAverageScore(statisticsDto.getAverageScore());

        return statistics;
    }
}
