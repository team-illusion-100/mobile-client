package be.kdg.mvmobile.model.games;

import java.time.LocalDateTime;

public class Notification {
    private int notificationId;
    private String content;
    private String title;
    private LocalDateTime timestamp;

    public Notification() {
    }

    public Notification(int notificationId, String content, String title, LocalDateTime timestamp) {
        this.notificationId = notificationId;
        this.content = content;
        this.title = title;
        this.timestamp = timestamp;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
