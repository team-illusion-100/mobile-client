package be.kdg.mvmobile.model.games;

import android.graphics.Color;

public class CharacterCard {
    private int id;
    private String characterName;
    private String imgPath;
    private Color extraGoldColor;

    public CharacterCard() {
    }

    public CharacterCard(int id, String characterName, String imgPath, Color extraGoldColor) {
        this.id = id;
        this.characterName = characterName;
        this.imgPath = imgPath;
        this.extraGoldColor = extraGoldColor;
    }

    public CharacterCard(String characterName, String imgPath, Color extraGoldColor) {
        this.characterName = characterName;
        this.imgPath = imgPath;
        this.extraGoldColor = extraGoldColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Color getExtraGoldColor() {
        return extraGoldColor;
    }

    public void setExtraGoldColor(Color extraGoldColor) {
        this.extraGoldColor = extraGoldColor;
    }
}
