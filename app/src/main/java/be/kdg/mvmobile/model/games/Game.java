package be.kdg.mvmobile.model.games;

import java.util.List;

import be.kdg.mvmobile.model.users.ChatMessage;

public class Game {
    private int id;
    private GameStatus status;
    private String name;
    private int coinsLeft;
    private int turnDuration;
    private int reqBuildings;
    private int maxPlayers;
    private JoinType joinType;
    private int playerTurn;
    private List<Round> rounds;                         //Will be used for the Replay
    private List<Player> players;                     //These are the participating players
    private List<BuildingCard> buildings;               //This is the stack of cards in the middle
    private List<CharacterCard> characters;               //These are the characters of the game
    private List<ChatMessage> chatMessages;
    private List<Notification> notifications;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCoinsLeft() {
        return coinsLeft;
    }

    public void setCoinsLeft(int coinsLeft) {
        this.coinsLeft = coinsLeft;
    }

    public int getTurnDuration() {
        return turnDuration;
    }

    public void setTurnDuration(int turnDuration) {
        this.turnDuration = turnDuration;
    }

    public int getReqBuildings() {
        return reqBuildings;
    }

    public void setReqBuildings(int reqBuildings) {
        this.reqBuildings = reqBuildings;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public void setJoinType(JoinType joinType) {
        this.joinType = joinType;
    }

    public int getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(int playerTurn) {
        this.playerTurn = playerTurn;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<BuildingCard> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<BuildingCard> buildings) {
        this.buildings = buildings;
    }

    public List<CharacterCard> getCharacters() {
        return characters;
    }

    public void setCharacters(List<CharacterCard> characters) {
        this.characters = characters;
    }

    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
}
