package be.kdg.mvmobile.model.games;

import android.graphics.Color;

public class BuildingCard {
    private int buildingCardId;
    private String buildingName;
    private int cost;
    private boolean isBought;
    private String imgPath;
    private Color color;

    public BuildingCard() {
    }

    public BuildingCard(int buildingCardId, String buildingName, int cost, boolean isBought, String imgPath, Color color) {
        this.buildingCardId = buildingCardId;
        this.buildingName = buildingName;
        this.cost = cost;
        this.isBought = isBought;
        this.imgPath = imgPath;
        this.color = color;
    }

    public int getBuildingCardId() {
        return buildingCardId;
    }

    public void setBuildingCardId(int buildingCardId) {
        this.buildingCardId = buildingCardId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isBought() {
        return isBought;
    }

    public void setBought(boolean bought) {
        isBought = bought;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
