package be.kdg.mvmobile.model.users;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    private int userId;
    private String username;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String deviceId;
    private Date dateOfBirth;
    private int numberOfGames;
    private int numberOfWins;
    private double avgScore;
    private int topScore;
    private int replayLifeDays;
    private List<Replay> replays;
    private List<Friend> friends;
    private String avatar;

    public User() {
        replays = new ArrayList<>();
        friends = new ArrayList<>();
    }

    public User(String username) {
        this.userId = -1;
        this.username = username;
        this.email = "";
        this.dateOfBirth = new Date();
        this.numberOfGames = 0;
        this.numberOfWins = 0;
        this.avgScore = 0;
        this.topScore = 0;
        this.replayLifeDays = 0;
        this.replays = new ArrayList<>();
        this.friends = new ArrayList<>();
    }

    public User(String username, String email, Date dateOfBirth) {
        this.userId = -1;
        this.username = username;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.numberOfGames = 0;
        this.numberOfWins = 0;
        this.avgScore = 0;
        this.topScore = 0;
        this.replayLifeDays = 0;
        this.replays = new ArrayList<>();
        this.friends = new ArrayList<>();
    }

    public User(int userId, String username, String email, String password, String firstName, String lastName, Date dateOfBirth, int numberOfGames, int numberOfWins, double avgScore, int topScore, int replayLifeDays, String avatar) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.numberOfGames = numberOfGames;
        this.numberOfWins = numberOfWins;
        this.avgScore = avgScore;
        this.topScore = topScore;
        this.replayLifeDays = replayLifeDays;
        this.avatar = avatar;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    public int getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(int numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(double avgScore) {
        this.avgScore = avgScore;
    }

    public int getTopScore() {
        return topScore;
    }

    public void setTopScore(int topScore) {
        this.topScore = topScore;
    }

    public int getReplayLifeDays() {
        return replayLifeDays;
    }

    public void setReplayLifeDays(int replayLifeDays) {
        this.replayLifeDays = replayLifeDays;
    }

    public void addReplay(Replay replay) {
        this.replays.add(replay);
    }

    public Replay getReplay(int id) {
        return replays.get(id);
    }

    public List<Replay> getReplays() {
        return replays;
    }

    public void addFriend(Friend friend) {
        this.friends.add(friend);
    }

    public Friend getFriend(int id) {
        return friends.get(id);
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
