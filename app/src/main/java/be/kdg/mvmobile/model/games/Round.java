package be.kdg.mvmobile.model.games;

public class Round {
    private int roundId;
    private int roundNr;

    public Round() {
    }

    public Round(int roundId, int roundNr) {
        this.roundId = roundId;
        this.roundNr = roundNr;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    public int getRoundNr() {
        return roundNr;
    }

    public void setRoundNr(int roundNr) {
        this.roundNr = roundNr;
    }
}
