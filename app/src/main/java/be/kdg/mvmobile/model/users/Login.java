package be.kdg.mvmobile.model.users;


/**
 * This class represents a user's login. In this class we will keep his access token and username,
 * but not his password. This class is also used to persist this data in DBFlow
 */
public class Login {
    private String username;
    private String access_token;

    public Login() {
    }

    public Login(String username) {
        this.username = username;
    }

    public Login(String username, String access_token) {
        this.username = username;
        this.access_token = access_token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
