package be.kdg.mvmobile.activities.mystatistics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.error.ErrorHandler;
import be.kdg.mvmobile.injection.DaggerAppComponent;
import be.kdg.mvmobile.services.UserService;
import be.kdg.mvmobile.services.dto.StatisticsDto;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyStatisticsActivity extends AppCompatActivity {
    private static final String TAG = "MyStatisticsActivity";

    // UI References
    @BindView(R.id.mystatistics_toolbar) Toolbar toolbar;
    @BindView(R.id.mystatistics_numberofgames) TextView numberOfGamesView;
    @BindView(R.id.mystatistics_numberofwins) TextView numberOfWinsView;
    @BindView(R.id.mystatistics_topscore) TextView topscoreView;
    @BindView(R.id.mystatistics_avgscore) TextView avgscoreView;

    @Inject
    UserService userService;

    private StatisticsDto statisticsDto;
    private MySharedPreferences mySharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_statistics);

        // Tell Butterknife to resolve the @BindView() annotations
        ButterKnife.bind(this);

        // Tell Dagger to inject the @Inject annotations
        DaggerAppComponent.builder().build().inject(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize SharedPreferences
        mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));
        getMyStatistics();
    }

    private void initialiseViews() {
        this.numberOfGamesView.setText(String.valueOf(statisticsDto.getNumberOfGames()));
        this.numberOfWinsView.setText(String.valueOf(statisticsDto.getNumberOfWins()));
        this.topscoreView.setText(String.valueOf(statisticsDto.getTopScore()));
        this.avgscoreView.setText(String.valueOf(statisticsDto.getAverageScore()));
    }

    private void getMyStatistics() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // Check whether the user is connected to the INTERNET, if not then show a Toast
        if (networkInfo != null && networkInfo.isConnected()) {
            Call<StatisticsDto> getStatisticsResponse =
                    this.userService.getStatistics("Bearer " + mySharedPreferences.readTokenSP().getAccess_token()
                            , mySharedPreferences.readUserSP().getUsername());
            getStatisticsResponse.enqueue(new Callback<StatisticsDto>() {
                @Override
                public void onResponse(Call<StatisticsDto> call, Response<StatisticsDto> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            statisticsDto = response.body();
                            initialiseViews();
                        } else {
                            ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_body_is_null), TAG, getApplicationContext());
                        }
                    } else {
                        ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_not_successful), TAG, getApplicationContext());
                    }
                }

                @Override
                public void onFailure(Call<StatisticsDto> call, Throwable t) {
                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), "getStatistics onFailure: " + t.getMessage(), TAG, getApplicationContext());
                }
            });
        } else {
            ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "", TAG, this);
        }
    }
}
