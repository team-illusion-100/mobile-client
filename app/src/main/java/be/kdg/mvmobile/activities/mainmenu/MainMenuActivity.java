package be.kdg.mvmobile.activities.mainmenu;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.io.IOException;

import javax.annotation.Nullable;
import javax.inject.Inject;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.activities.friends.FriendListActivity;
import be.kdg.mvmobile.activities.game.GameActivity;
import be.kdg.mvmobile.activities.mystatistics.MyStatisticsActivity;
import be.kdg.mvmobile.activities.rules.RulesActivity;
import be.kdg.mvmobile.activities.start.StartActivity;
import be.kdg.mvmobile.error.ErrorHandler;
import be.kdg.mvmobile.injection.DaggerAppComponent;
import be.kdg.mvmobile.model.users.User;
import be.kdg.mvmobile.services.UserService;
import be.kdg.mvmobile.services.WebsocketService;
import be.kdg.mvmobile.services.dto.StatisticsDto;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainMenuActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbarView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerView;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @Nullable
    @BindView(R.id.nav_header_username)
    TextView navHeaderUsernameView;
    @BindView(R.id.mainmenu_button_connect_game)
    Button connectButton;
    @BindView(R.id.mainmenu_edittext_gameid)
    EditText gamIdEditText;

    @Inject
    UserService userService;

    // SharedPreferences
    private MySharedPreferences mySharedPreferences;

    private WebsocketService websocketService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        setSupportActionBar(toolbarView);
        ButterKnife.bind(this);

        DaggerAppComponent.builder().build().inject(this);
        mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));
        WebsocketService websocketService = new WebsocketService(mySharedPreferences);
        User user = mySharedPreferences.readUserSP();
        if (user != null) {
            isTokenExpired(user);
        }

        initialiseViews();
        addEventHandlers();
    }

    private void isTokenExpired(User user) {
        // To check whether the token is expired we do a call to the backend to see whether we get a
        // 200 or a 401. If we get a 401 we know the token has expired!
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // Check whether the user is connected to the INTERNET, if not then show a Toast
        if (networkInfo != null && networkInfo.isConnected()) {
            Call<StatisticsDto> getStatisticsResponse = userService.getStatistics("Bearer " + mySharedPreferences.readTokenSP().getAccess_token(), user.getUsername());
            getStatisticsResponse.enqueue(new Callback<StatisticsDto>() {
                @Override
                public void onResponse(Call<StatisticsDto> call, Response<StatisticsDto> response) {
                    if (response.isSuccessful()) {
                        // This means we received a 200, don't need do to anything here then!

                    } else {
                        // This means we received a 40x, now just need to check whether it was about an
                        // expired token
                        try {
                            if (response.errorBody().string().contains("invalid_token")) {
                                // Token is expired, log out the user and send him to the start screen

                                // Clear mySharedPreferences so that the stored Token and User get deleted
                                mySharedPreferences.clearAllSP();

                                // Send the user back to the Start screen as a new task and clear all the
                                // other activities so they can't come back to the main menu or other
                                // previously visited activities
                                Intent intent = new Intent(getApplicationContext(), StartActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        } catch (IOException e) {
                            ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "", TAG, getApplicationContext());
                        }
                    }
                }

                @Override
                public void onFailure(Call<StatisticsDto> call, Throwable t) {
                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong),
                            "getStatistics onFailure: " + t.getMessage(), TAG, getApplicationContext());
                }
            });
        } else {
            ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "", TAG, this);

            // User is not connected to internet so can't check the expiration of the token so we will
            // just log him out
            // Token is expired, log out the user and send him to the start screen

            // Clear mySharedPreferences so that the stored Token and User get deleted
            mySharedPreferences.clearAllSP();

            // Send the user back to the Start screen as a new task and clear all the
            // other activities so they can't come back to the main menu or other
            // previously visited activities
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private void initialiseViews() {
        User user = mySharedPreferences.readUserSP();
        View header = navigationView.getHeaderView(0);
        TextView text = (TextView) header.findViewById(R.id.nav_header_username);
        ImageView imageView = (ImageView) header.findViewById(R.id.imageView);
        header.setBackground(getDrawable(R.drawable.fort_background));

        text.setText(user.getUsername());

        switch (user.getAvatar()) {
            case "avatar1":
                imageView.setImageResource(R.mipmap.ic_avatar1_round);
                break;
            case "avatar2":
                imageView.setImageResource(R.mipmap.ic_avatar2_round);
                break;
            case "avatar3":
                imageView.setImageResource(R.mipmap.ic_avatar3_round);
                break;
            case "avatar4":
                imageView.setImageResource(R.mipmap.ic_avatar4_round);
                break;
            case "avatar5":
                imageView.setImageResource(R.mipmap.ic_avatar5_round);
                break;
            case "avatar6":
                imageView.setImageResource(R.mipmap.ic_avatar6_round);
                break;
            case "avatar7":
                imageView.setImageResource(R.mipmap.ic_avatar7_round);
                break;
            case "avatar8":
                imageView.setImageResource(R.mipmap.ic_avatar8_round);
                break;
            case "avatar9":
                imageView.setImageResource(R.mipmap.ic_avatar9_round);
                break;
            case "avatar10":
                imageView.setImageResource(R.mipmap.ic_avatar10_round);
                break;
        }
    }

    private void addEventHandlers() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerView, toolbarView, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerView.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        connectButton.setOnClickListener(v -> {
            websocketService = new WebsocketService(this.mySharedPreferences);
            websocketService.initialiseWebSocketConnection();
            int gameId=Integer.valueOf(gamIdEditText.getText().toString());
            mySharedPreferences.writeConnectedGameIdSP(gameId);
            websocketService.getStompClient().send("/app/android/connect/" + gameId).subscribe();
            websocketService.getStompClient().topic("/r/android/connect/" + mySharedPreferences.readUserSP().getUsername() + "/" + gameId).subscribe(stompMessage -> {
                if (stompMessage.getPayload().equals("true")) {
                    Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                    startActivity(intent);
                } else {
                    this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Je hebt geen toegang tot dit spel, join eerst een spel via de website", Toast.LENGTH_LONG).show());
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerView.isDrawerOpen(GravityCompat.START)) {
            drawerView.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_friends) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), FriendListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_statistics) {
            Intent intent = new Intent(getApplicationContext(), MyStatisticsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_rules) {
            Intent intent = new Intent(getApplicationContext(), RulesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            // Clear mySharedPreferences so that the stored Token and User get deleted
            mySharedPreferences.clearAllSP();

            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        drawerView.closeDrawer(GravityCompat.START);
        return true;
    }
}
