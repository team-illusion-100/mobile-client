package be.kdg.mvmobile.activities.friends;

import android.app.Activity;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import be.kdg.mvmobile.R;
import be.kdg.mvmobile.model.users.Friend;
import be.kdg.mvmobile.model.users.User;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

/**
 * A fragment representing a single Friend detail screen.
 * This fragment is either contained in a {@link FriendListActivity}
 * in two-pane mode (on tablets) or a {@link FriendDetailActivity}
 * on handsets.
 */
public class FriendDetailFragment extends Fragment {
    // UI references
    @BindView(R.id.frienddetail_collapsingtoolbar_layout) CollapsingToolbarLayout collapsingToolbarLayout;

    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The Friend content this fragment is presenting.
     */
    private Friend mItem;
    private int mItemIndex;

    // SharedPreferences
    private MySharedPreferences mySharedPreferences;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FriendDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this.getActivity());

        // Initialize SharedPreferences
        mySharedPreferences = new MySharedPreferences(this.getActivity(), getContext().getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            User user = mySharedPreferences.readUserSP();

            for (Friend friend : user.getFriends()) {
                if (Integer.parseInt(getArguments().getString(ARG_ITEM_ID)) == friend.getFriendId()) {
                    mItem = friend;
                    mItemIndex = user.getFriends().indexOf(mItem);
                }
            }

            if (collapsingToolbarLayout != null) {
                collapsingToolbarLayout.setTitle(mItem.getUsername());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.friend_detail, container, false);

        if (mItem != null) {
            User user = mySharedPreferences.readUserSP();
            ((TextView) rootView.findViewById(R.id.friend_detail_username)).setText(mItem.getUsername());
            ((TextView) rootView.findViewById(R.id.friend_detail_email)).setText(mItem.getEmail());
            ((TextView) rootView.findViewById(R.id.friend_detail_numberofgames)).setText(Integer.toString(mItem.getStatistics().getNumberOfGames()));
            ((TextView) rootView.findViewById(R.id.friend_detail_numberofwins)).setText(Integer.toString(mItem.getStatistics().getNumberOfWins()));
            ((TextView) rootView.findViewById(R.id.friend_detail_topscore)).setText(Integer.toString(mItem.getStatistics().getTopScore()));
            ((TextView) rootView.findViewById(R.id.friend_detail_avgscore)).setText(Double.toString(mItem.getStatistics().getAverageScore()));
        }

        return rootView;
    }
}
