package be.kdg.mvmobile.activities.login;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.activities.mainmenu.MainMenuActivity;
import be.kdg.mvmobile.error.ErrorHandler;
import be.kdg.mvmobile.injection.DaggerAppComponent;
import be.kdg.mvmobile.model.users.Login;
import be.kdg.mvmobile.model.users.Token;
import be.kdg.mvmobile.model.users.User;
import be.kdg.mvmobile.services.UserService;
import be.kdg.mvmobile.services.dto.DeviceDto;
import be.kdg.mvmobile.services.dto.DtoMapper;
import be.kdg.mvmobile.services.dto.UserDto;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import be.kdg.mvmobile.util.UtilProgress;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    @Inject
    UserService userService;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private Login mAuthTask = null;

    // UI references.
    @BindView(R.id.loginactivity_edittext_username) EditText usernameview;
    @BindView(R.id.loginactivity_edittext_password) EditText passwordView;
    @BindView(R.id.loginactivity_button_sign_in) Button buttonSignInView;
    @BindView(R.id.loginactivity_progressbar) View progressBarView;
    @BindView(R.id.loginactivity_scrollview_form) View loginFormView;

    // SharedPreferences
    private MySharedPreferences mySharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Tell Butterknife to resolve the @BindView() annotations
        ButterKnife.bind(this);

        // Tell Dagger to inject the @Inject annotations
        DaggerAppComponent.builder().build().inject(this);

        // Initialize SharedPreferences
        mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));

        addEventHandlers();
    }

    private void addEventHandlers() {
        buttonSignInView.setOnClickListener(view -> attemptLogin());
    }

    private boolean isUsernameValid(String username) {
        return username.length() > 1;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid username, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        usernameview.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        String username = usernameview.getText().toString();
        String password = passwordView.getText().toString();

        // Variable cancel is so we can stop the login attempt if form validation fails
        boolean cancel = false;
        // Variable focusView so we can give the focus to the view that caused an error
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            passwordView.setError(getString(R.string.loginactivity_error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }

        // Check for an empty username field and a valid username.
        if (TextUtils.isEmpty(username)) {
            usernameview.setError(getString(R.string.loginactivity_error_field_required));
            focusView = usernameview;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            usernameview.setError(getString(R.string.loginactivity_error_invalid_username));
            focusView = usernameview;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            UtilProgress.showProgress(true, this, loginFormView, progressBarView);
            mAuthTask = new Login(username);
            performLogin();
        }
    }

    /**
     * This method takes care of the networking aspect of logging in
     * 3 calls are done in this method:
     *  - login()           --> to request a token from the backend and log the user in
     *  - getUser()         --> to get a UserDto object back from the backend so we can fill up the User in SP
     *  - sendDeviceId()    --> to send the deviceId to the backend so we can send push notifications to specific devices
     */
    private void performLogin() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        DtoMapper dtoMapper = new DtoMapper();

        // Check whether the user is connected to the INTERNET, if not then show a Toast
        if (networkInfo != null && networkInfo.isConnected()) {
            Call<Token> loginResponse = userService.login(usernameview.getText().toString(), passwordView.getText().toString(), "password");

            loginResponse.enqueue(new Callback<Token>() {
                @Override
                public void onResponse(Call<Token> call, Response<Token> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Token token = new Token(response.body().getAccess_token(), response.body().getToken_type(), response.body().getExpires_in(), response.body().getScope(), response.body().getJti());

                            Call<UserDto> getUserResponse = userService.getUser("Bearer " + token.getAccess_token());
                            getUserResponse.enqueue(new Callback<UserDto>() {
                                @Override
                                public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                                    if (response.isSuccessful()) {
                                        if (response.body() != null) {
                                            // Get the deviceId so we can add it to the user object and save it
                                            FirebaseInstanceId.getInstance().getInstanceId()
                                                    .addOnCompleteListener(task -> {
                                                        if (!task.isSuccessful()) {
                                                            ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong),
                                                                    "getInstanceId failed" + task.getException(), TAG, getApplicationContext());
                                                            resetLoginAttempt();
                                                            return;
                                                        }

                                                        // Get new Instance ID token
                                                        String deviceToken = task.getResult().getToken();

                                                        // Log and toast
                                                        String msg = "Test: " + deviceToken;
                                                        Log.d(TAG, msg);

                                                        // Map the Dto object to the model class User
                                                        User user = dtoMapper.userFromDto(response.body());
                                                        user.setDeviceId(deviceToken);

                                                        // Write the token and user to SharedPreferences
                                                        mySharedPreferences.writeTokenSP(token);
                                                        mySharedPreferences.writeUserSP(user);

                                                        mAuthTask.setAccess_token(token.getAccess_token());

                                                        // Set up the DeviceDto object
                                                        DeviceDto deviceDto = new DeviceDto();
                                                        deviceDto.setUsername(user.getUsername());
                                                        deviceDto.setDeviceId(deviceToken);

                                                        Call<Void> sendDeviceIdResponse = userService.sendDeviceId("Bearer " + token.getAccess_token(), deviceDto);
                                                        sendDeviceIdResponse.enqueue(new Callback<Void>() {
                                                            @Override
                                                            public void onResponse(Call<Void> call1, Response<Void> response1) {
                                                                if (response1.isSuccessful()) {
                                                                    // Create a new Intent to start the MainMenuActivity with
                                                                    // the intention to make it a new task and to clear all
                                                                    // previous tasks so the user can't go back to his previous
                                                                    // activities
                                                                    Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                    startActivity(intent);
                                                                } else {
                                                                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_not_successful), TAG, getApplicationContext());
                                                                    resetLoginAttempt();
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<Void> call1, Throwable t) {
                                                                ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), "sendDeviceId onFailure: " + t.getMessage(), TAG, getApplicationContext());
                                                                resetLoginAttempt();
                                                            }
                                                        });
                                                    });
                                        } else {
                                            ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_body_is_null), TAG, getApplicationContext());
                                            resetLoginAttempt();
                                        }
                                    } else {
                                        ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_not_successful), TAG, getApplicationContext());
                                        resetLoginAttempt();
                                    }
                                }

                                @Override
                                public void onFailure(Call<UserDto> call, Throwable t) {
                                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), "getUser onFailure: " + t.getMessage(), TAG, getApplicationContext());
                                    resetLoginAttempt();
                                }
                            });
                        } else {
                            ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_body_is_null), TAG, getApplicationContext());
                            resetLoginAttempt();
                        }
                    } else {
                        ErrorHandler.handleError(getString(R.string.loginactivity_error_incorrect_password), getString(R.string.retrofit_error_not_successful), TAG, getApplicationContext());
                        resetLoginAttempt();
                    }
                }

                @Override
                public void onFailure(Call<Token> call, Throwable t) {
                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), "login onFailure: " + t.getMessage(), TAG, getApplicationContext());
                    resetLoginAttempt();
                }
            });
        } else {
            ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "", TAG, this);
            resetLoginAttempt();
        }
    }

    /**
     * In this method we will reset the login attempt and show the form back to the user instead of
     * the progressbar
     */
    private void resetLoginAttempt() {
        UtilProgress.showProgress(false, this, loginFormView, progressBarView);
        passwordView.setText("");
        mAuthTask = null;
    }
}