package be.kdg.mvmobile.activities.friends;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.recyclerview.widget.RecyclerView;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.error.ErrorHandler;
import be.kdg.mvmobile.injection.DaggerAppComponent;
import be.kdg.mvmobile.model.users.Friend;
import be.kdg.mvmobile.model.users.Statistics;
import be.kdg.mvmobile.model.users.User;
import be.kdg.mvmobile.services.FriendService;
import be.kdg.mvmobile.services.UserService;
import be.kdg.mvmobile.services.dto.DtoMapper;
import be.kdg.mvmobile.services.dto.FriendDto;
import be.kdg.mvmobile.services.dto.StatisticsDto;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendListActivity extends AppCompatActivity {
    private static final String TAG = "FriendListActivity";

    // UI References
    @BindView(R.id.friendlist_toolbar) Toolbar toolbarView;
    @BindView(R.id.friend_list) View recyclerView;

    // Services
    @Inject
    FriendService friendService;
    @Inject
    UserService userService;

    // MySharedPreferences
    private MySharedPreferences mySharedPreferences;

    private List<Friend> myFriends = new ArrayList<>();
    private Statistics statistics;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        ButterKnife.bind(this);

        DaggerAppComponent.builder().build().inject(this);

        // Initialize SharedPreferences
        mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));

        setSupportActionBar(toolbarView);
        toolbarView.setTitle(getTitle());

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (findViewById(R.id.friend_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        User user = mySharedPreferences.readUserSP();
        myFriends = user.getFriends();
        DtoMapper dtoMapper = new DtoMapper();

        FriendListActivity activity = this;
        FriendItemRecyclerViewAdapter adapter = new FriendItemRecyclerViewAdapter(activity, myFriends, mTwoPane);
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        recyclerView.setAdapter(adapter);

        // Check whether the user is connected to the INTERNET, if not then show a Toast
        if (networkInfo != null && networkInfo.isConnected()) {
            Call<List<FriendDto>> getFriendsResponse = friendService.getFriends("Bearer " + mySharedPreferences.readTokenSP().getAccess_token());
            getFriendsResponse.enqueue(new Callback<List<FriendDto>>() {
                @Override
                public void onResponse(Call<List<FriendDto>> call, Response<List<FriendDto>> response) {
                    if (response.isSuccessful()) {
                        List<Friend> newFriends = new ArrayList<>();
                        statistics = null;
                        for (FriendDto friendDto : response.body()) {
                            Call<StatisticsDto> getStatisticsResponse = userService.getStatistics("Bearer " + mySharedPreferences.readTokenSP().getAccess_token(), mySharedPreferences.readUserSP().getUsername());
                            getStatisticsResponse.enqueue(new Callback<StatisticsDto>() {
                                @Override
                                public void onResponse(Call<StatisticsDto> call, Response<StatisticsDto> response) {
                                    if (response.isSuccessful()) {
                                        statistics = dtoMapper.statisticsFromDto(response.body());
                                        Friend friend = dtoMapper.friendFromDto(friendDto);

                                        friend.setStatistics(statistics);
                                        newFriends.add(friend);

                                        user.setFriends(newFriends);
                                        mySharedPreferences.writeUserSP(user);

                                        adapter.setFriendsList(newFriends);
                                    } else {
                                        ErrorHandler.handleError(getString(R.string.toast_error_not_connected), getString(R.string.retrofit_error_not_successful), TAG, getApplicationContext());
                                    }
                                }

                                @Override
                                public void onFailure(Call<StatisticsDto> call, Throwable t) {
                                    ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "getStatistics onFailure: " + t.getMessage(), TAG, getApplicationContext());
                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<FriendDto>> call, Throwable t) {
                    ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "getFriends onFailure: " + t.getMessage(), TAG, getApplicationContext());
                }
            });
        } else {
            ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "", TAG, this);
        }
    }
}
