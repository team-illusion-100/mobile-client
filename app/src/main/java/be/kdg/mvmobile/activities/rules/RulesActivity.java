package be.kdg.mvmobile.activities.rules;

import android.os.Bundle;


import com.github.barteksc.pdfviewer.PDFView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import be.kdg.mvmobile.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RulesActivity extends AppCompatActivity {
    // UI references
    @BindView(R.id.rules_toolbar) Toolbar toolbar;
    @BindView(R.id.pdfView) PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        // Tell Butterknife to resolve the @BindView() annotations
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pdfView.fromAsset("gebruikershandleiding-com.pdf").load();
    }
}
