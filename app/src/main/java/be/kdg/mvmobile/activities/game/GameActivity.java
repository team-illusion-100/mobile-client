package be.kdg.mvmobile.activities.game;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import be.kdg.mvmobile.R;
import be.kdg.mvmobile.services.WebsocketService;
import be.kdg.mvmobile.services.dto.AndroidDataDto;
import be.kdg.mvmobile.services.dto.BuildingCardDto;
import be.kdg.mvmobile.services.dto.CharacterCardDto;
import be.kdg.mvmobile.services.dto.GameDto;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import io.reactivex.disposables.Disposable;

public class GameActivity extends Activity {
    private WebsocketService websocketService;
    private MySharedPreferences mySharedPreferences;
    private Gson gson = new GsonBuilder().create();
    private int gameId;
    private LinearLayout linearLayout;
    private TextView acitonTitle;
    private List<BuildingCardDto> buildingCardDtos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        this.linearLayout = findViewById(R.id.game_content);
        this.acitonTitle = new TextView(this);
        this.acitonTitle.setText(R.string.game_actiontitle_wait_turn);
        this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
        this.linearLayout.addView(this.acitonTitle);
        mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));
        websocketService = new WebsocketService(mySharedPreferences);
        websocketService.initialiseWebSocketConnection();
        this.gameId = mySharedPreferences.readConnectedGameIdSP();
        myTurnHandler();
    }


    private void myTurnHandler() {
        Disposable disposable1 = websocketService.getStompClient().topic("/r/android/buildingCards/" + mySharedPreferences.readUserSP().getUsername() + "/" + this.gameId).subscribe(stompMessage -> {
            AndroidDataDto androidDataDto = gson.fromJson(stompMessage.getPayload(), AndroidDataDto.class);
            this.buildingCardDtos = androidDataDto.getBuildingCards();
        });
        Disposable disposable = websocketService.getStompClient().topic("/r/android/myTurn/" + mySharedPreferences.readUserSP().getUsername() + "/" + this.gameId).subscribe(stompMessage -> {
            // If a message is received here, it means it's this player's turn. It contains a json with isChooseCardTurn and the full game state.
            AndroidDataDto androidDataDto = gson.fromJson(stompMessage.getPayload(), AndroidDataDto.class);
            GameDto game = androidDataDto.getGameDto();
            if (androidDataDto.getIsChooseCardTurn().equals("true")) {

                runOnUiThread(() -> {
                    this.acitonTitle.setText(R.string.game_actiontitle_choose_character);
                    this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                    this.linearLayout.removeAllViews();
                    this.linearLayout.addView(this.acitonTitle);
                });

                for (CharacterCardDto c : game.getCharacterCards()) {
                    runOnUiThread(() -> {
                        Button characterBtn = new Button(this);
                        characterBtn.setText(c.getCharacter().getCharacterName());
                        characterBtn.setId(c.getId());
                        this.addCharacterBtnEventHandler(characterBtn, c);
                        this.linearLayout.addView(characterBtn);
                    });
                }
            } else if (androidDataDto.getIsChooseCardTurn().equals("false")) {
                runOnUiThread(() -> {
                    this.acitonTitle.setText(R.string.game_actiontitle_what_action);
                    this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                    Button takeCoinsBtn = new Button(this);
                    takeCoinsBtn.setText(R.string.game_actiontitle_take2coins);
                    this.addCoinsBtnEventHandler(takeCoinsBtn);
                    Button takeCardsBtn = new Button(this);
                    takeCardsBtn.setText(R.string.game_actiontitle_draw2cards);
                    this.addCardsBtnEventHandler(takeCardsBtn);
                    this.linearLayout.removeAllViews();
                    this.linearLayout.addView(this.acitonTitle);
                    this.linearLayout.addView(takeCoinsBtn);
                    this.linearLayout.addView(takeCardsBtn);
                });
            }
        });
    }


    private void addCharacterBtnEventHandler(Button characterBtn, CharacterCardDto characterCardDto) {
        characterBtn.setOnClickListener(v -> {
            this.websocketService.getStompClient().send("/app/android/characterSelected/" + gameId, gson.toJson(characterCardDto)).subscribe();
            runOnUiThread(() -> {
                this.acitonTitle.setText(R.string.game_actiontitle_wait_turn);
                this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                this.linearLayout.removeAllViews();
                this.linearLayout.addView(this.acitonTitle);
            });
        });
    }

    private void addCoinsBtnEventHandler(Button takeCoinsBtn) {
        takeCoinsBtn.setOnClickListener(v -> {
            this.websocketService.getStompClient().send("/app/android/coinsClick/" + gameId).subscribe();
            runOnUiThread(() -> {
                this.acitonTitle.setText(R.string.game_actiontitle_wait_turn);
                this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                this.linearLayout.removeAllViews();
                this.linearLayout.addView(this.acitonTitle);
            });
        });
    }

    private void addCardsBtnEventHandler(Button takeCardsBtn) {
        takeCardsBtn.setOnClickListener(v -> {
            this.websocketService.getStompClient().send("/app/android/cardsClick/" + gameId).subscribe();
            runOnUiThread(() -> {
                this.acitonTitle.setText(R.string.game_actiontitle_keep_card);
                this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                Button card1Btn = new Button(this);
                card1Btn.setText(this.buildingCardDtos.get(0).getBuilding().getBuildingName());
                this.addSelectCardBtnEventHandler(card1Btn, this.buildingCardDtos.get(0));
                Button card2Btn = new Button(this);
                card2Btn.setText(this.buildingCardDtos.get(1).getBuilding().getBuildingName());
                this.addSelectCardBtnEventHandler(card2Btn, this.buildingCardDtos.get(1));
                this.linearLayout.removeAllViews();
                this.linearLayout.addView(this.acitonTitle);
                this.linearLayout.addView(card1Btn);
                this.linearLayout.addView(card2Btn);
            });
        });
    }

    private void addSelectCardBtnEventHandler(Button cardBtn, BuildingCardDto buildingCardDto) {
        cardBtn.setOnClickListener(v -> {
            this.websocketService.getStompClient().send("/app/android/buildingSelected/" + gameId, gson.toJson(buildingCardDto)).subscribe();
            runOnUiThread(() -> {
                this.acitonTitle.setText(R.string.game_actiontitle_wait_turn);
                this.acitonTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                this.linearLayout.removeAllViews();
                this.linearLayout.addView(this.acitonTitle);
            });
        });
    }
}
