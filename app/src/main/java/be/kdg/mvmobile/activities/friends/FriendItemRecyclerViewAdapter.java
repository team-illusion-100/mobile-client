package be.kdg.mvmobile.activities.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.model.users.Friend;

public class FriendItemRecyclerViewAdapter extends RecyclerView.Adapter<FriendItemRecyclerViewAdapter.ViewHolder> {
    private final FriendListActivity mParentActivity;
    private List<Friend> mValues;
    private final boolean mTwoPane;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Friend item = (Friend) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(FriendDetailFragment.ARG_ITEM_ID, Integer.toString(item.getFriendId()));
                FriendDetailFragment fragment = new FriendDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.friend_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, FriendDetailActivity.class);
                intent.putExtra(FriendDetailFragment.ARG_ITEM_ID, Integer.toString(item.getFriendId()));

                context.startActivity(intent);
            }
        }
    };

    FriendItemRecyclerViewAdapter(FriendListActivity parent,
                                  List<Friend> items,
                                  boolean twoPane) {
        mValues = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    public void setFriendsList(List<Friend> friends) {
        this.mValues = friends;
        notifyDataSetChanged();
    }

    @Override
    public FriendItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friend_list_content, parent, false);
        return new FriendItemRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FriendItemRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mContentView.setText(mValues.get(position).getUsername());

        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mContentView;

        ViewHolder(View view) {
            super(view);
            mContentView = (TextView) view.findViewById(R.id.friendlist_content_name);
        }
    }
}
