package be.kdg.mvmobile.error;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ErrorHandler {
    /**
     * When called, this method will take the provided parameters to show a Toast to the user and to
     * log the error
     *
     * @param toastMessage This message will be shown in a Toast to the user, if the message is an
     *                     empty String then no Toast will be shown to the user
     * @param logMessage This message will be logged as an error, if the message is an empty String
     *                   then nothing will be logged
     * @param tag This parameter represents the tag that will be given with the Log.e()
     * @param context The Context where the Toast needs to be shown
     */
    public static void handleError(String toastMessage, String logMessage, String tag, Context context) {
        if (!toastMessage.isEmpty()) {
            Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
        }

        if (!logMessage.isEmpty()) {
            Log.e(tag, logMessage);
        }
    }
}
